%
% \brief save_blender_config saves the calculated plenoptic camera data to
%                       csv file compatible with the Blender add-on
%
% \param lensfile       name of the file containing the loaded main lens 
%                       data
% \param target_file    path of the file to write the data into - leave
%                       empty to open file dialog
%
function save_blender_config(lensfile, target_file)

    % if no target file is given, open file dialog and ask user for target
    % file
    if nargin < 2
        filter = {'*.csv'};
        [target_file, target_path] = uiputfile(filter);
        target_file = strcat(target_path, target_file);
    end

    % get relevant global variables for main lens, MLA and sensor
    global a_main b_main d_main;
    global a_ML b_ML d_ML f_ML;
    global pixel_size sensor_height;
    
    % create and open the file
    file = fopen(target_file,'w');
    
    % write parameters
    fprintf(file,'%s;%s\n', "objective_file_name", lensfile);
    fprintf(file,'%s;%f\n', "prop_objective_scale",1.0);
    fprintf(file,'%s;%i\n', "prop_vertex_count_radial",32);
    fprintf(file,'%s;%i\n', "prop_vertex_count_height",36);
    fprintf(file,'%s;%i\n', "prop_aperture_blades",12);
    fprintf(file,'%s;%f\n', "prop_aperture_size",d_main);
    fprintf(file,'%s;%f\n', "prop_aperture_angle",30);
    fprintf(file,'%s;%f\n', "prop_sensor_width",sensor_height);
    fprintf(file,'%s;%f\n', "prop_sensor_height",sensor_height);
    fprintf(file,'%s;%f\n', "prop_pixel_size",pixel_size);
    fprintf(file,'%s;%f\n', "prop_wavelength",587.5999755859375);
    fprintf(file,'%s;%f\n', "prop_focus_distance",a_main/10);
    fprintf(file,'%s;%f\n', "prop_sensor_mainlens_distance",...
        b_main+a_ML+b_ML);
    fprintf(file,'%s;%s\n', "prop_mla_enabled","True");
    fprintf(file,'%s;%s\n', "prop_mla_type","HEX");
    fprintf(file,'%s;%f\n', "prop_microlens_diam",d_ML*1000);
    fprintf(file,'%s;%f\n', "prop_mla_sensor_dist",b_ML);
    fprintf(file,'%s;%s\n', "prop_three_ml_types","False");
    fprintf(file,'%s;%f\n', "prop_ml_type_1_f",f_ML);
    fprintf(file,'%s;%f\n', "prop_ml_type_2_f",f_ML);
    fprintf(file,'%s;%f\n', "prop_ml_type_3_f",f_ML);

    % close the file
    fclose(file);
    
end