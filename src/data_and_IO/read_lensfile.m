%
% \brief read_lensfile loads the main lens data
%
% \param file_path     path to the lens data .csv file
%
function read_lensfile(file_path)

    % read lensfile
    global radius thickness material index v_no sa;
    global num_lens_surfaces;
    fileID = fopen(file_path);
    parsed = textscan(fileID , '%f %f %s %f %f %f','Delimiter',';',...
        'EmptyValue',0,'CommentStyle','radius');
    fclose(fileID);

    % split data into suitably named arrays
    radius = parsed{1};
    thickness = parsed{2};
    material = parsed{3};
    index = parsed{4};
    index(index == 0) = 1;
    v_no = parsed{5};
    sa = parsed{6};

    % calculate IOR ratios for backward and forward tracing
    num_lens_surfaces = size(index,1);
    global IOR_forward IOR_backward;
    IOR_forward = index;
    IOR_forward(1) = 1 / index(1);
    for i = 2:num_lens_surfaces
        IOR_forward(i) = index(i-1) / index(i);
    end

    IOR_backward = index;
    for i = 2:num_lens_surfaces
        IOR_backward(i) = index(i) / index(i-1);
    end

    % calculate lens centers and aperture index
    global position aperture_index f_main;
    position = zeros(num_lens_surfaces,1);
    aperture_index = 0;
    f_main = 0;
    calc_lens_centers(radius, thickness, material, sa);
end
