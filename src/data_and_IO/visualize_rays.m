%
% \brief visualize_rays plots rays for debug purposes
%
% \param rays           cell array containing the ray points
% \param mirror         if true, the rays are mirrored at the optical axis
%
function visualize_rays(rays, mirror)
    
    if nargin < 2
        mirror = false;
    end

    hold on;
    [num_rays, ~] = size(rays);
    for i = 1:num_rays
        [num_ray_points, ~] = size(rays{i});
        if num_ray_points > 0
            if mirror
                plot(rays{i}(:,1), -rays{i}(:,2));
            else
                plot(rays{i}(:,1), rays{i}(:,2));
            end
        end
    end
end