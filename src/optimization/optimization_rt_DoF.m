%
% \brief optimization_rt_DoF calculates the camera parameters 
%                   according to the ray tracing measurements for a single
%                   scene point to be in focus
%
function optimization_rt_DoF()

    global DoF f_main f_ML lambda pixel_size; % in
    global d_ML; % in but will be optimized
    global a_main b_main d_main a_ML b_ML d_ML; % out
    global m; % derived
    
    % initial scene side object distance as center of DoF
    a_main = (DoF(1) + DoF(2)) / 2;
    DoF_center = (DoF(1) + DoF(2)) / 2;
    DoF_size = DoF(2) - DoF(1);
    % initial parameter estimation
    optimization_rt_single_focus();
    % calculate intial DoF
    [current_DoF, current_DoF_size] = calc_DoF_optical_axis_rt();
    current_DoF_center = (current_DoF(1) + current_DoF(2)) / 2;
    
    threshold_DoF = 0.005; % 1 percent of DoF size is acceptable
    threshold_DoF_center = 0.01;
    threshold_DoF_size = 0.01;
    
    best_d_ML = d_ML;
    best_DoF_size = current_DoF_size;
    best_DoF_center = current_DoF_center;
    
    %fprintf('[%d, %d]\n',current_DoF(1),current_DoF(2));
    
    global sa aperture_index;
    d_main_max = sa(aperture_index) * 2;
    
    % start optimization
    while (abs(current_DoF(1) - DoF(1)) > DoF(1) * threshold_DoF || ...
            abs(current_DoF(2) - DoF(2)) > DoF(2) * threshold_DoF) && ...
            best_d_ML > 5 * pixel_size && d_main < d_main_max
        
        % optimize DoF size by modifying d_ML
        within_threshold = (abs(best_DoF_size - DoF_size) < ...
                    threshold_DoF_size);
        if ~within_threshold && best_d_ML > 5 * pixel_size
            for step_size_exp = 1:3
                
                d_ML = best_d_ML + sign(best_DoF_size - ...
                    DoF_size) * 0.1^step_size_exp;

                if d_ML <= 5 * pixel_size
                    continue
                end

                % parameter estimation
                optimization_rt_single_focus();
                % calculate DoF
                [current_DoF, current_DoF_size] = calc_DoF_optical_axis_rt();

                DoF_better = (abs(current_DoF_size - DoF_size) < ...
                    abs(best_DoF_size - DoF_size));
                if DoF_better
                    fprintf('[%d, %d]\n',current_DoF(1),current_DoF(2));
                    best_d_ML = d_ML;
                    best_DoF_size = current_DoF_size;
                    best_DoF_center = (current_DoF(1)+current_DoF(2))/2;
                    break
                elseif d_main > d_main_max
                    continue
                end
                
                switch_sign = (sign(current_DoF_size - DoF_size) ~= ...
                        sign(best_DoF_size - DoF_size));
                    
                if switch_sign
                    current_delta = current_DoF_size - DoF_size;
                    best_delta = best_DoF_size - DoF_size;
                    
                    if current_delta < best_delta
                        min_delta = current_delta;
                        max_delta = best_delta;
                        min_best_d_ML = best_d_ML;
                        max_best_d_ML = d_ML;
                    else
                        min_delta = best_delta;
                        max_delta = current_delta;
                        min_best_d_ML = d_ML;
                        max_best_d_ML = best_d_ML;
                    end
                    
                    for halving = 1:10
                        
                        if abs(max_best_d_ML - min_best_d_ML) < 0.001
                            break;
                        end
                        
                        d_ML = (min_best_d_ML + max_best_d_ML) / 2;
                        
                        % parameter estimation
                        optimization_rt_single_focus();
                        % calculate DoF
                        [current_DoF, current_DoF_size] = calc_DoF_optical_axis_rt();

                        DoF_better = (abs(current_DoF_size - DoF_size) < ...
                            abs(best_DoF_size - DoF_size));
                        if DoF_better
                            fprintf('[%d, %d]\n',current_DoF(1),current_DoF(2));
                            best_d_ML = d_ML;
                            best_DoF_size = current_DoF_size;
                            best_DoF_center = (current_DoF(1)+current_DoF(2))/2;
                            break
                        else
                            current_delta = current_DoF_size - DoF_size;
                            if current_delta < 0
                                min_delta = current_delta;
                                max_best_d_ML = d_ML;
                            elseif current_delta > 0
                                max_delta = current_delta;
                                min_best_d_ML = d_ML;
                            end
                        end
                        
                    end
                    
                end
                
                if DoF_better
                    break
                end
            end
        end
        
        if d_main > d_main_max
            break
        end
        
        % optimize DoF center by modifying a_main
        d_ML = best_d_ML;
        best_a_main = a_main;

        within_threshold = (abs(current_DoF_center - DoF_center)...
                    < threshold_DoF_center);
        if ~within_threshold
            for step_size_exp = 0:4
                a_main = best_a_main - sign(best_DoF_center - ...
                    DoF_center) * 100 * 0.1^step_size_exp;

                if a_main <= 0
                    continue
                end

                % parameter estimation
                optimization_rt_single_focus();
                % calculate DoF
                [current_DoF, current_DoF_size] = calc_DoF_optical_axis_rt();
                current_DoF_center = (current_DoF(1) + current_DoF(2)) / 2;

                DoF_center_better = (abs(current_DoF_center - DoF_center) < ...
                    abs(best_DoF_center - DoF_center));
                if DoF_center_better
                    fprintf('[%d, %d]\n',current_DoF(1),current_DoF(2));
                    best_a_main = a_main;
                    best_DoF_size = current_DoF_size;
                    best_DoF_center = current_DoF_center;
                    break
                end
            end
        end
    end
    
    assert(best_d_ML > 5 * pixel_size, 'The current ML diameter is below the size of 5 px. This setup is not useful. Please consider decreasing the DoF or increasing the DoF distance to the camera');
    assert(d_main < d_main_max, 'The current aperture size is above allowed maximum. This setup is not useful. Please consider increasing the DoF or decreasing the DoF distance to the camera');

    
    % final parameter estimation
    a_main = best_a_main;
    d_ML = best_d_ML;
    % initial parameter estimation
    optimization_rt_single_focus();
end