%
% \brief optimization_rt_single_focus calculates the camera parameters 
%                   according to the ray tracing measurements for a single
%                   scene point to be in focus
%
function optimization_rt_single_focus()

    global a_main f_main f_ML d_ML lambda; % in
    global b_main d_main a_ML b_ML; % out
    global m; % derived
    
    %%%%%%%%%%  Initial aperture estimation via lensmaker model  %%%%%%%%%%
    
    [~, d_main, ~, ~] = calc_all_lensmaker(a_main, f_main, f_ML, ...
        d_ML, lambda);

    %%%%%%%%%%%%%  Calculate exact focus point b_main via RT  %%%%%%%%%%%%%

    % calculate focus point paraxial and minimum blur
    [b_paraxial, ray_points_paraxial, ray_directions_paraxial] = ...
        calc_focus_point_paraxial_rt(a_main);

    [b_min_blur, ray_points_min_blur, ray_directions_min_blur] = ...
        calc_focus_point_min_blur_rt(a_main);

    % approximation of focus point via image plane criteria listed in Geary
    % chapter 33.5
    delta_MB_BV_ratio = 0.531/1.5;
    b_main = (1 - delta_MB_BV_ratio) * b_paraxial + ...
        delta_MB_BV_ratio * b_min_blur;
    
    %%%%%%%%%%%%%%  Reinitialization with correct focus point  %%%%%%%%%%%%
    
    a_ML = calc_a_ML_thin_lens(f_ML, b_main, lambda);
    b_ML = calc_b_ML_thin_lens(a_ML, b_main, lambda);
    d_main = calc_d_main_lensmaker(b_main, a_ML, b_ML, d_ML);
    
    %%%%%%%%%%%%%%%%%%%  Optimization of a_ML and b_ML  %%%%%%%%%%%%%%%%%%%
    
    thresh_lambda = 0.01;
    thresh_focus = 0.01;
    
    % calculate current sensor position
    current_sensor = b_main + a_ML + b_ML;
    
    % calculate current magnification
    m = calc_magnification_rt();
    
    % estimate current focus point
    current_focus_parax = calc_ML_focus_point_paraxial_rt(...
        ray_points_paraxial, ray_directions_paraxial);
    current_focus_min_blur = calc_ML_focus_point_min_blur_rt(...
        ray_points_min_blur, ray_directions_min_blur);
    current_focus_ML = (1 - delta_MB_BV_ratio) * current_focus_parax + ...
        delta_MB_BV_ratio * current_focus_min_blur;
    
    % estimate current disparity coefficient
    current_lambda = calc_disparity_rt(b_main);
    
    step_size_exp = 1;
    
    while (abs(current_sensor - current_focus_ML) > thresh_focus) || ...
            (abs(current_lambda - lambda) > thresh_lambda)
        
        delta_lambda = abs(lambda - current_lambda);
        delta_focus = abs(current_sensor - current_focus_ML);
        
        old_a_ML = a_ML;
        old_b_ML = b_ML;
        
        if lambda < current_lambda
            if current_sensor < current_focus_ML
                % increase a_ML
                a_ML = a_ML + 0.1^step_size_exp;
            else
                % decrease b_ML
                b_ML = b_ML - 0.1^step_size_exp;
            end   
        else
            if current_sensor < current_focus_ML
                % increase b_ML
                b_ML = b_ML + 0.1^step_size_exp;
            else
                % decrease a_ML
                a_ML = a_ML - 0.1^step_size_exp;
            end 
        end
        
        % calculate new sensor position
        new_sensor = b_main + a_ML + b_ML;

        % calculate new magnification
        new_m = calc_magnification_rt();

        % estimate new focus point
        current_focus_parax = calc_ML_focus_point_paraxial_rt(...
            ray_points_paraxial, ray_directions_paraxial);
        current_focus_min_blur = calc_ML_focus_point_min_blur_rt(...
            ray_points_min_blur, ray_directions_min_blur);
        new_focus_ML = (1 - delta_MB_BV_ratio) * current_focus_parax + ...
            delta_MB_BV_ratio * current_focus_min_blur;

        % estimate new disparity coefficient
        new_lambda = calc_disparity_rt(b_main);
        
        new_delta_focus = abs(new_sensor - new_focus_ML);
        new_delta_lamba = abs(new_lambda - lambda);
        if (new_delta_focus > delta_focus && new_delta_focus > thresh_focus) || ...
                (new_delta_lamba > delta_lambda && new_delta_lamba > thresh_lambda)
            % reset values and decrease step_size
            a_ML = old_a_ML;
            b_ML = old_b_ML;
            step_size_exp = step_size_exp + 1;
        else
            current_sensor = new_sensor;
            m = new_m;
            current_focus_ML = new_focus_ML;
            current_lambda = new_lambda;
        end
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%  Optimization of d_main  %%%%%%%%%%%%%%%%%%%%%%
    
    best_d_main = d_main;
    for step_size_exp = 0:3
        MLI_too_large = true;
        while MLI_too_large
            d_main = best_d_main - 0.1^step_size_exp;
            vis_MLI_size = calc_visible_MLI_rt();
            MLI_too_large = (vis_MLI_size > m * d_ML);
            if MLI_too_large
                best_d_main = d_main;
            end
        end
    end
    
end