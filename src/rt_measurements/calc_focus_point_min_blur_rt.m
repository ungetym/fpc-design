%
% \brief calc_focus_point_min_blur_rt calculates the minimum blur 
%                               spot for b_main
%
% \param a_main                 distance between scene point and main lens
% \param debug_visualization    enables/disables visualization
%
% \return b_main                minimum blur approximation for b_main 
% \return ray_points            positions of traced ray intersections
% \return ray_directions        directions of the rays at these positions
%
function [b_main, ray_points, ray_directions] = calc_focus_point_min_blur_rt(...
    a_main, debug_visualization)

    if nargin < 2
        debug_visualization = false;
    end

    global f_main position radius sa sensor_response_func;

    % set ray starting position
    p = [-a_main; 0];

    % calculate maximum ray angle with respect to the x-axis that is not 
    % blocked by objective
    max_direction = [position(1) - radius(1) + a_main; sa(1)];
    max_angle = abs(calc_angle([1,0], max_direction));
    current_angle = max_angle;
    best_angle = 0;
    for i = 1:20
        [~, ~, ~, blocked] = trace_through_objective(p,...
            [cos(current_angle);sin(current_angle)], true);
        if ~blocked
            best_angle = current_angle;
            current_angle = current_angle + (1/2^i)*max_angle;
            if current_angle > max_angle
                break
            end
        else
            current_angle = current_angle - (1/2^i)*max_angle;
        end
    end

    % trace rays from scene into camera
    num_rays = 50;
    ray_points = cell(num_rays, 1);
    ray_directions = cell(num_rays, 1);
    for i = 1:num_rays
        current_angle = (i - 1)/(num_rays - 1) * best_angle;
        [x, y, v, blocked] = trace_through_objective(p,...
            [cos(current_angle);sin(current_angle)], true);
        if ~blocked
            % extend ray after last lens using the back focal point estimation 
            % from Smith's 'Modern Lens Design'
            y = [y; y(end)+(2*f_main-x(end))/v(1)*v(2)];
            x = [x; 2*f_main];
            % save rays
            ray_points{i} = [x,y];
            ray_directions{i} = v;
        end
    end

    % for debugging plot the traced rays
    if debug_visualization
        figure;
        title("Tracing from scene into camera for focus calculation");
        visualize_rays(ray_points);
        visualize_rays(ray_points, true);
    end

    % calculate line representations and weights
    m = zeros(num_rays, 1);
    b = zeros(num_rays, 1);
    w = zeros(num_rays, 1);
    w(1) = sensor_response_func(0);
    for i = 2:num_rays
        m(i) = (ray_points{i}(end, 2) - ray_points{i}(end-1, 2)) / ...
            (ray_points{i}(end, 1) - ray_points{i}(end-1, 1));
        b(i) = ray_points{i}(end-1, 2) - (ray_points{i}(end-1, 1) * m(i));
        % weight ray according to sensor response
        w(i) = sensor_response_func(asin(abs(m(i))));
    end
    
    % calculate focus point as minimum ray envelope cross section, i.e. the
    % position of minimum blur radius
    min_ray_idx = 1;
    max_ray_idx = 1;
    % next intersection positions of max and min ray
    next_x_max = inf;
    next_x_min = inf;
    % calculate next intersection of min ray - this is the first ray
    % crossing the optical axis and will be used as starting point
    for i = 1:num_rays
        if i ~= min_ray_idx
            x_min = (b(min_ray_idx) - b(i)) / (m(i) - m(min_ray_idx));
            if x_min < next_x_min
                next_x_min = x_min;
                next_min_idx = i;
            end
        end
    end
    current_x = x_min;
    
    % find initial ray with maximum height at lens exit
    max_y = 0;
    for i = 2:num_rays
        y = m(i) * current_x + b(i);
        if y > max_y
            max_ray_idx = i;
            max_y = y;
        end
    end
    
    
    % the following loop will jump to the next intersection position of
    % either max or min array depending on which is closer to the current
    % position
    
    % true if position moved to next max intersection in last iteration
    next_to_calc_is_max = true;
    % smallest blur radius given by the ray envelope cross section
    smallest_section = inf;
    
    decreasing = true;
    while decreasing
        % calculate next max ray intersection
        if next_to_calc_is_max
            for i = 1:num_rays
                if i ~= max_ray_idx
                    x_max = (b(max_ray_idx) - b(i)) / (m(i) - m(max_ray_idx));
                    if x_max > current_x && x_max < next_x_max
                        next_x_max = x_max;
                        next_max_idx = i;
                    end
                end
            end
        % calculate next min ray intersection
        else
            for i = 1:num_rays
                if i ~= min_ray_idx
                    x_min = (b(min_ray_idx) - b(i)) / (m(i) - m(min_ray_idx));
                    if x_min > current_x && x_min < next_x_min
                        next_x_min = x_min;
                        next_min_idx = i;
                    end
                end
            end
        end
        
        % check, which intersection is closest to the current position
        if next_x_max < next_x_min
            % go on with max step
            current_x = next_x_max;
            next_x_max = inf;
            max_ray_idx = next_max_idx;
            next_to_calc_is_max = true;
        else
            % go on with min step
            current_x = next_x_min;
            next_x_min = inf;
            min_ray_idx = next_min_idx;
            next_to_calc_is_max = false;
        end
        
        % calculate ray envelope cross section
        section = max(abs((m(max_ray_idx) * current_x + b(max_ray_idx))), ...
             abs(m(min_ray_idx) * current_x + b(min_ray_idx)));
        
        % check whether the current blur radius is smaller than the
        % previous one
        decreasing = (section < smallest_section);
        if decreasing
            smallest_section = section;
            b_main = current_x;
        end
    end
    
end