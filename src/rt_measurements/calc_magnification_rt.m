%
% \brief calc_magnification_rt calculates magnification between sensor and
%                               MLA
%
% \return                       magnification factor
%
function magnification = calc_magnification_rt()

    global aperture_index b_main a_ML b_ML sensor_height;
    d_objective_sensor = b_main + a_ML + b_ML;

    % calculate magnification factor between ML centers and MLI centers by
    % tracing rays from the center of the aperture into the camera
    
    current_angle = calc_angle([d_objective_sensor;sensor_height],[1;0]);
    blocked = true;
    magnification = 1;
    while blocked
        current_angle = current_angle / 2;
        direction = [cos(current_angle); sin(current_angle)];
        [x,y,w,blocked] = trace_through_objective([0;0], direction, true,...
            aperture_index);
        if ~blocked
            [x_MLA,y_MLA, w, blocked] = trace_through_two_plane_MLA(...
                [x(end);y(end)], w, true, 1);
            if ~blocked
                % calculate sensor hit by solving p+sv=(d_objective_sensor,*)
                s = (d_objective_sensor - x_MLA(end)) / w(1);
                sensor_hit = y_MLA(2) + s * w(2);
                if abs(2 * sensor_hit) < sensor_height
                    magnification = sensor_hit / y_MLA(2);
                    break
                else
                    blocked = true;
                end
            end
        end
    end
end