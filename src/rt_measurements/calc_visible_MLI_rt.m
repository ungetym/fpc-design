function d_MLI = calc_visible_MLI_rt()

    global b_main a_ML b_ML d_ML sensor_max_angle m;
    
    % calculate center pixel response
    center_px_angle = calc_angle([-1;0],[-b_ML; d_ML/2]);
    
    center_response = 2 * approximate_sensor_response_integral(0, ...
        center_px_angle * 180 / pi);
    %center_response =2*integral(@(angle) calc_sensor_response(angle,1.4,1), ...
    %    0, center_px_angle,"ArrayValued", true);
    
    % trace rays from sensor through MLA and objective to find minimum 
    % angle of incident light for these sensor pixels
    sensor_position = [b_main + a_ML + b_ML; 0];

    for step_size = 0:5
        receives_light = true;
        while receives_light
            
            %current_position = sensor_position + [0; d_ML * 0.5^step_size];
            current_position = sensor_position + [0; 0.1];
            
            for j = -sensor_max_angle:1:sensor_max_angle
                direction = [cos(pi + j*pi/180); sin(pi + j*pi/180)];
                [x,y,w,blocked] = trace_through_two_plane_MLA(...
                    current_position, direction, false, 2);
                
                if 2 * abs(y(2)) > d_ML
                    blocked = true;
                end
                
                if blocked
                    continue
                end
                [~,~,~,blocked] = trace_through_objective(...
                    [x(end);y(end)], w, false);

                if ~blocked
                    min_angle_idx = j;
                    break
                end
            end
            
            if j == sensor_max_angle
                break
            end
            
            for j = sensor_max_angle:-1:min_angle_idx
                direction = [cos(pi + j*pi/180); sin(pi + j*pi/180)];
                [x,y,w,blocked] = trace_through_two_plane_MLA(...
                    current_position, direction, false, 2);
                
                if blocked || 2 * abs(y(2)) > d_ML
                    continue
                end

                [~,~,~,blocked] = trace_through_objective(...
                    [x(end);y(end)], w, false);
                if ~blocked
                    max_angle_idx = j;
                    break
                end
            end
            
            if j == min_angle_idx
                break
            end
            
            % integrate sensor response function
            current_response = approximate_sensor_response_integral(...
                min_angle_idx, max_angle_idx);
%             current_response = integral(@(angle) ...
%                     calc_sensor_response(angle,1.4,1), min_angle_idx * ...
%                     pi/180, max_angle_idx*pi/180,"ArrayValued", true);
            receives_light = (center_response < 25 * current_response);
            if receives_light
                sensor_position = current_position;
            end
            
            if step_size > 0
                break;
            end
        end
    end
    
    d_MLI = 2 * sensor_position;
end