%
% \brief calc_ML_focus_point_paraxial_rt calculates the paraxial 
%                               approximation for the sensor position        
%
% \param ray_points             paraxial traced rays points
% \param ray_directions         paraxial traced rays positions
%
% \return                       paraxial approximation        
%
function focus_ML_paraxial = calc_ML_focus_point_paraxial_rt(...
    ray_points, ray_directions)

    global d_ML

    num_rays = size(ray_points,1);
    focus_ML_paraxial = 0;
    num_sensor_hits = 0;
    for i = 1:num_rays
        % trace rays through MLA
        [x, y, v, blocked] = trace_through_two_plane_MLA(...
            ray_points{i}(end,:), ray_directions{i}, true, 2);
        if ~blocked && (2*abs(y(end)) < d_ML)
            % calculate mean of optical axis intersections
            focus_ML_paraxial = focus_ML_paraxial + ...
                1/num_rays * (x(end)-y(end)*v(1)/v(2));
            num_sensor_hits = num_sensor_hits + 1;
        end
    end
    focus_ML_paraxial = focus_ML_paraxial * num_rays / num_sensor_hits;
end