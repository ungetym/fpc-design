%
% \brief calc_DoF_off_axis_rt calculates the DoF for an off-center pixel
%
% \param pixel_position     the pixels distance from the sensor center in
%                           mm
%
% \return                   [pixel position, focus point, DoF size, 
%                           DoF min, DoF max]                    
%
function DoF_data = calc_DoF_off_axis_rt(pixel_position)
    
    global d_objective_sensor;
    global diam_ML;
    global pixel_size;
    global sensor_response_func;

    %%%%%%%%%  Restrict incident angles using the sensor response %%%%%%%%%
    
    % calculate maximum incident light angle for pixel to have a 
    % significant response (more than 1% of 0 degree incidence response)
    response_at_0 = sensor_response_func(0);
    current_response = response_at_0;
    max_angle = 0;
    while (current_response/response_at_0 > 0.01)
        max_angle = max_angle + 0.1 * pi / 180;
        current_response = sensor_response_func(max_angle);
    end
    
    %%%%%%%%%%%%%%%%  Calculate mean scene ray for pixel  %%%%%%%%%%%%%%%%%
    
    % find min and max direction for pixel to receive light from
    best_angle_max = pi + max_angle;
    pixel = [d_objective_sensor; pixel_position];
    for angle_step = 0:5
        receives_light = false;
        while ~receives_light && best_angle_max >= pi - max_angle
            current_angle = best_angle_max - 0.1^angle_step * pi/180;
            direction = [cos(current_angle); sin(current_angle)];
            [x_MLA,y_MLA, w, blocked] = trace_through_two_plane_MLA(...
                    pixel, direction, false, 3);
            if ~blocked
                [~,~,~,blocked] = trace_through_objective(...
                    [x_MLA(end);y_MLA(end)], w, false);
            end
            if blocked
                best_angle_max = current_angle;
            end
            receives_light = ~blocked;
        end
        
        if best_angle_max < pi - max_angle
            DoF_data = [];
            return;
        end
        
    end
    best_angle_max = current_angle;

    best_angle_min = pi - max_angle;
    for angle_step = 0:5
        receives_light = false;
        while ~receives_light && best_angle_min <= pi + max_angle
            current_angle = best_angle_min + 0.1^angle_step * pi/180;
            direction = [cos(current_angle); sin(current_angle)];
            [x_MLA,y_MLA, w, blocked] = trace_through_two_plane_MLA(...
                    pixel, direction, false, 3);
            if ~blocked
                [~,~,~,blocked] = trace_through_objective(...
                    [x_MLA(end);y_MLA(end)], w, false);
            end
            if blocked
                best_angle_min = current_angle;
            end
            receives_light = ~blocked;
        end
        
        if best_angle_min > pi + max_angle
            DoF_data = [];
            return;
        end
    end
    best_angle_min = current_angle;
    
    mean_angle = 0;
    sum_weight = 0;
    best_weight = 0;
    for j = 0:100
        current_angle = j/100 * best_angle_min + (100-j)/100 * best_angle_max;
        weight = sensor_response_func(current_angle-pi);
        if weight > best_weight
            mean_angle = current_angle;
            best_weight = weight;
        end
        %sum_weight = sum_weight + weight;
        %mean_angle = mean_angle + weight * current_angle;
    end
    %mean_angle = mean_angle / sum_weight;
    
    % trace mean ray to determine scene ray for DoF starting points
    %mean_angle = 0.5*(best_angle_max + best_angle_min);
    direction = [cos(mean_angle); sin(mean_angle)];
    [x_MLA,y_MLA, w, blocked] = trace_through_two_plane_MLA(pixel, ...
        direction, false, 3);
    if ~blocked
        [x,y,v,blocked] = trace_through_objective(...
            [x_MLA(end);y_MLA(end)], w, false);
        if ~blocked
            mean_ray = [[x_MLA;x(2:end)],[y_MLA;y(2:end)]];
            p = [x(end);y(end)];
            v = v/norm(v);
        end
    end
    
    if blocked
        DoF_data = [];
        return;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%  Find focus point  %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % trace 10 rays within min and max direction to approximate focus point
    s_focus = 0;
    sum_weight = 0;
    for j = 0:9
        current_angle = j/9 * best_angle_min + (9-j)/9 * best_angle_max;
        direction = [cos(current_angle); sin(current_angle)];
        [x_MLA,y_MLA, w, blocked] = trace_through_two_plane_MLA(...
                    pixel, direction, false, 3);
        if ~blocked
            [x,y,w,blocked] = trace_through_objective(...
                [x_MLA(end);y_MLA(end)], w, false);
            if ~blocked
                q = [x(end);y(end)];
                % calculate intersection with aperture center ray
                denom = v(1)*w(2) - v(2)*w(1);
                if denom ~= 0
                    s = (w(1)*(p(2)-q(2))-w(2)*(p(1)-q(1))) / denom;
                    % calculate weighted mean according to sensor response
                    weight = sensor_response_func(current_angle-pi);
                    sum_weight = sum_weight + weight;
                    s_focus = s_focus + weight * s;
                end
            end
        end
    end

    s_focus = s_focus / sum_weight;

    %%%%%%%%%%%%%%%%%%%%%%%%  Calculate DoF area  %%%%%%%%%%%%%%%%%%%%%%%%%
    
    % trace rays around focus point to calculate DoF for current pixel
    ML_center = diam_ML * floor((mean_ray(3,2) + 0.5 * diam_ML) / diam_ML);

    s_min = s_focus;
    for step_d = 0:5
        within_one_pixel = true;
        while within_one_pixel
            s_current = s_min - 10 * 0.5^step_d;
            point = p+s_current*v;
            max_angle = calc_angle(-v, [1;0]);
            for step_angle = 0:10
                hits_sensor = true;
                while hits_sensor && within_one_pixel
                    current_angle = max_angle + 10 * 0.5^step_angle * pi/180;
                    [x,y,w,blocked] = trace_through_objective(...
                        point, ...
                        [cos(current_angle);sin(current_angle)], true);
                    if ~blocked
                        [x_MLA,y_MLA,w_MLA,blocked] = trace_through_two_plane_MLA(...
                            [x(end);y(end)], w, true, 3);

                        if 2 * abs(y_MLA(2) - ML_center) > diam_ML
                            blocked = true;
                        end

                        if ~blocked
                            % calculate sensor hit by solving p+sv = 
                            % (d_objective_sensor,*)
                            s = (d_objective_sensor - x_MLA(end)) / w_MLA(1);
                            sensor_hit = y_MLA(2) + s * w_MLA(2);
                            within_one_pixel = (2*abs(sensor_hit-pixel(2))<pixel_size);
                            max_angle = current_angle;
                        end   
                    end
                    hits_sensor = ~blocked;
                end
            end

            min_angle = calc_angle(-v, [1;0]);
            for step_angle = 0:10
                hits_sensor = true;
                while hits_sensor && within_one_pixel
                    current_angle = min_angle - 10 * 0.5^step_angle * pi/180;
                    [x,y,w,blocked] = trace_through_objective(...
                        point, ...
                        [cos(current_angle);sin(current_angle)], true);
                    if ~blocked
                        [x_MLA,y_MLA,w_MLA,blocked] = trace_through_two_plane_MLA(...
                            [x(end);y(end)], w, true, 3);

                        if 2 * abs(y_MLA(2) - ML_center) > diam_ML
                            blocked = true;
                        end

                        if ~blocked
                            % calculate sensor hit by solving p+sv = 
                            % (d_objective_sensor,*)
                            s = (d_objective_sensor - x_MLA(end)) / w_MLA(1);
                            sensor_hit = y_MLA(2) + s * w_MLA(2);
                            within_one_pixel = (2*abs(sensor_hit-pixel(2))<pixel_size);
                            min_angle = current_angle;
                        end   
                    end
                    hits_sensor = ~blocked;
                end
            end

            if within_one_pixel
                s_min = s_current;
            end
        end
    end

    s_max = s_focus;
    for step_d = 0:5
        within_one_pixel = true;
        while within_one_pixel && (s_max - s_focus) < 10*(s_focus - s_min)
            s_current = s_max + 10 * 0.5^step_d;
            point = p+s_current*v;

            max_angle = calc_angle(-v, [1;0]);
            for step_angle = 0:10
                hits_sensor = true;
                while hits_sensor
                    current_angle = max_angle + 10 * 0.5^step_angle * pi/180;
                    [x,y,w,blocked] = trace_through_objective(...
                        point, ...
                        [cos(current_angle);sin(current_angle)], true);
                    if ~blocked
                        [x_MLA,y_MLA,w_MLA,blocked] = trace_through_two_plane_MLA(...
                            [x(end);y(end)], w, true, 3);

                        if 2 * abs(y_MLA(2) - ML_center) > diam_ML
                            blocked = true;
                        end

                        if ~blocked
                            % calculate sensor hit by solving p+sv = 
                            % (d_objective_sensor,*)
                            s = (d_objective_sensor - x_MLA(end)) / w_MLA(1);
                            sensor_hit = y_MLA(2) + s * w_MLA(2);
                            within_one_pixel = (2*abs(sensor_hit-pixel(2))<pixel_size);
                            max_angle = current_angle;
                        end   
                    end
                    hits_sensor = ~blocked;
                end
            end

            min_angle = calc_angle(-v, [1;0]);
            for step_angle = 0:10
                hits_sensor = true;
                while hits_sensor && within_one_pixel
                    current_angle = min_angle - 10 * 0.5^step_angle * pi/180;
                    [x,y,w,blocked] = trace_through_objective(...
                        point, ...
                        [cos(current_angle);sin(current_angle)], true);
                    if ~blocked
                        [x_MLA,y_MLA,w_MLA,blocked] = trace_through_two_plane_MLA(...
                            [x(end);y(end)], w, true, 3);

                        if 2 * abs(y_MLA(2) - ML_center) > diam_ML
                            blocked = true;
                        end

                        if ~blocked
                            % calculate sensor hit by solving p+sv = 
                            % (d_objective_sensor,*)
                            s = (d_objective_sensor - x_MLA(end)) / w_MLA(1);
                            sensor_hit = y_MLA(2) + s * w_MLA(2);
                            within_one_pixel = (2*abs(sensor_hit-pixel(2))<pixel_size);
                            min_angle = current_angle;
                        end   
                    end
                    hits_sensor = ~blocked;
                end
            end

            if within_one_pixel
                s_max = s_current;
            end
        end
        
        if (s_max - s_focus) >= 10*(s_focus - s_min)
            s_max = inf;
            break
        end
    end
    
    DoF_data = [pixel_position, p(1)+s_focus*v(1), s_max-s_min,...
        (p+s_min*v).', (p+s_max*v).'];
end