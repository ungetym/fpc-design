%
% \brief calc_disparity_rt	calculates the disparity coefficient for a given
%                           virtual point
%
% \param p_virtual          axial position of the virtual point in mm
%
% \return                   disparity coefficient                   
%
function lambda = calc_disparity_rt(p_virtual)

    global b_main a_ML b_ML d_ML m;
    
    % calculate direction to trace ray from [p_virtual;0] through center of
    % the first off-center microlens
    v = [(b_main + a_ML) - p_virtual; d_ML];
    v = v/norm(v);
    
    % trace ray through MLA
    [x,y, w, blocked] = trace_through_two_plane_MLA( ...
        [p_virtual;0], v, true, 3);
    assert(~blocked, "Unable to calculate disparity");
    
    % calculate sensor hit
    hit = y(end) + (b_main+a_ML+b_ML - x(end)) * w(2)/w(1);
    
    % calculate lambda from hit to center distance and d_MLI
    lambda = (hit - m*d_ML) / (m*d_ML);
end