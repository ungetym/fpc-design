%
% \brief calc_angle calculates the signed angle from w to v (counter
%                   clockwise)
%
% \param v          first vector
% \param w          second vector
%
% \return           signed angle in radians
%
function a = calc_angle(v,w)
    a = atan2(v(2), v(1)) - atan2(w(2), w(1));
    if a > pi
        a = a - 2 * pi;
    elseif a < -pi
        a = a + 2 * pi;
    end
end
