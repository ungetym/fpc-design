clear all

image_dir = '../data/renderings_dof_4/';
%image_dir = '../renderings/';
image_list = dir(image_dir);
setup_names = {'26','27','28','29','30'};
method_names = {'dof'};
white_image_suffix = 'white';
num_imgs_per_set = 99;

num_curves = size(setup_names, 2) * size(method_names, 2);

contrast_data = zeros(2 * num_curves, num_imgs_per_set);
counter = -1;

for setup_idx = 1:size(setup_names, 2)
    for method_idx = 1:size(method_names, 2)

        counter = counter + 2;
        % load white image
        file_name_begin = append(setup_names{setup_idx}, '_', ...
            method_names{method_idx}, '_');
        file_name_end = '.png';
        white_image_name = append(file_name_begin, ...
            white_image_suffix, file_name_end);
        white_image = imread(append(image_dir, white_image_name));

        [img_width, img_height, ~] = size(white_image);
        
        min_px = round(0.2 * img_width);
        max_px = round(0.8 * img_width);
        
        %min_px = 4;
        %max_px = 37;

        white_image = white_image(min_px:max_px, min_px:max_px, 1);
        white_image = double(max(1, white_image)); % prevent div by zero

        filelist = image_list(startsWith({image_list.name}, ...
            file_name_begin));
        filelist = filelist(endsWith({filelist.name}, file_name_end));

        for img_idx = 1:size(filelist, 1)-1 %last is white image

            pattern_pos = filelist(img_idx).name(...
                size(file_name_begin,2)+1:...
                size(filelist(img_idx).name,2)-size(file_name_end,2));
            contrast_data(counter,img_idx) = str2double(pattern_pos);

            % load image
            current_image = imread(append(image_dir, ...
                filelist(img_idx).name));

            % get center region
            current_image = double(current_image(min_px:max_px, ...
                min_px:max_px, 1));

            % normalize
            current_image = current_image ./ white_image;

            % calculate mean of dark and bright values
            mean_value = mean(current_image);
            max_mean = mean(current_image(current_image > mean_value));
            min_mean = mean(current_image(current_image < mean_value));

            % calculate and save contrast
            if isnan(max_mean) || isnan(min_mean)
                contrast = 0;
            else
                contrast = max_mean - min_mean;
            end

            contrast_data(counter+1,img_idx) = contrast;
        end
    end
end



dofs = {[900, 1100], [1500, 2000], [500, 530], [950, 1150], [1450, 1750]};
legends={'D-Gauss 6, [900, 1100]','D-Gauss 7, [1500, 2000]', ...
    'D-Gauss 8 [500, 530]','Fisheye [950, 1150]', 'Fisheye [1450, 1750]'};
figure;
hold on;
plot_colors = jet(5);
for i = 1:5
    contrast_data(2*i-1:2*i,:) = sortrows(contrast_data(2*i-1:2*i,:)')';
    color = plot_colors(i, :);
    semilogx(contrast_data(2*i-1,:), contrast_data(2*i,:)/max(contrast_data(2*i,:)), 'Color', color, 'LineWidth', 3);
    legend(legends{i});
end
for i = 1:5
    color = plot_colors(i, :);
    semilogx([dofs{i}(1),dofs{i}(1)],[0,2],'Color', color, 'LineWidth', 3, 'LineStyle','--');
    semilogx([dofs{i}(2),dofs{i}(2)],[0,2],'Color', color, 'LineWidth', 3, 'LineStyle','--');
end
set(gca, 'XScale', 'log');
legend(legends{1},legends{2},legends{3},legends{4},legends{5});

clearvars -except contrast_data