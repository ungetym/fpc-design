%
% \brief auto_design_single_focus basically only puts the lens parsing, 
%                                 parameter calculation and config output
%                                 into a single function
%
function auto_design_single_focus(a_main_, f_ML_, d_ML_, lambda_, pixel_size_,...
    sensor_height_, lensfile_path, save_file, method)

    clearvars -except a_main_ f_ML_ d_ML_ lambda_ pixel_size_ ...
        sensor_height_ lensfile_path save_file lenses_path lensfiles ...
        method;

    %%%%%%%%%%%%%%%%%%%%%%%% Load objective data %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    data;
    read_lensfile(lensfile_path);

    %%%%%%%%%%%% Set desired parameters for thin lens estimation %%%%%%%%%%

    a_main = a_main_;
    f_ML = f_ML_;
    d_ML = d_ML_;
    lambda = lambda_;

    pixel_size = pixel_size_;
    sensor_height = sensor_height_;

    %%%%%%%%%%%%%%%%%%%% Calculate remaining parameters %%%%%%%%%%%%%%%%%%%

    if nargin<9
        method = 3;
    end
    
    if method == 1
        [b_main, d_main, a_ML, b_ML] = calc_all_thin_lens(a_main, f_main, ...
        f_ML, d_ML, lambda);
    elseif method == 2
        [b_main, d_main, a_ML, b_ML] = calc_all_lensmaker(a_main, f_main, ...
        f_ML, d_ML, lambda);
    else 
        optimization_rt_single_focus();
    end
        
    %%%%%%%%%%%%%%%%%%%% Calculate DoF for center pixel %%%%%%%%%%%%%%%%%%%

    %[DoF, DoF_size] = calc_DoF_optical_axis_rt();

    %%%%%%%%%%%%%%%%%%%%%% Save config and clean up %%%%%%%%%%%%%%%%%%%%%%%
    if nargin == 8
        [~, lensfile, file_ending] = fileparts(lensfile_path);
        save_blender_config(strcat(lensfile,file_ending), save_file);
    end

end