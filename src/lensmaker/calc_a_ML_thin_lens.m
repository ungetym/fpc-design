%
% \brief calc_a_ML_thin_lens calculates the distance between virtual image 
%                   and MLA according to the thin lens model
%
% \param f_ML       focal length of the microlens in mm
% \param b_main     distance between main lens and image side focus
%                   point in mm
% \param lambda     disparity coefficient
%
% \return           distance between virtual image and MLA in mm
%
function a_ML = calc_a_ML_thin_lens(f_ML, b_main, lambda)
    a_ML = -b_main/2+sqrt(f_ML*b_main*(1+lambda)/lambda + b_main*b_main/4);
end