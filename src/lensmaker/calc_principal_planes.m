%
% \brief calc_principal_planes calculates the positions of the two
%                   principal planes of the main lens by tracing rays 
%                   parallel to the optical axis - this of course requires
%                   the main lens to already be loaded
%
% \return p_1     	location of first principal plane
% \return p_2     	location of second principal plane
%
function [p_1, p_2] = calc_principal_planes()

    global a_main d_main;

    % trace ray from scene into camera to calculate p_2
    blocked = true;
    current_height = d_main;
    while blocked
        current_height = current_height/2;
        [x, y, v, blocked] = trace_through_objective(...
            [-a_main; current_height], [1;0], true);
    end
    % calculate p_2
    p_2 = x(end) + (current_height - y(end)) * v(1)/v(2);
    
    % trace ray from camera into scene to calculate p_1
    blocked = true;
    current_height = d_main;
    while blocked
        current_height = current_height/2;
        [x, y, v, blocked] = trace_through_objective(...
            [a_main; current_height], [-1;0], false);
    end
    % calculate p_2
    p_1 = x(end) + (current_height - y(end)) * v(1)/v(2);

end