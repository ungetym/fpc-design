%
% \brief calc_d_main_thin_lens calculates the aperture size of the main 
%                   lens
%
% \param d_ML       microlens diameter in mm
% \param b_main     distance between main lens and image side focus
%                   point in mm
% \param a_ML       distance between virtual image and MLA in mm
% \param b_ML    	distance between sensor and MLA in mm
%
% \return           main lens aperture size in mm
%
function d_main = calc_d_main_thin_lens(d_ML, b_main, a_ML, b_ML)
    d_main = d_ML * (b_main + a_ML) / b_ML;
end