%
% \brief b_main_thick_lens calculates the distance between virtual 
%                   image and main lens based on the thick lens model
%
% \param p_1     	location of first principal plane
% \param p_2        location of second principal plane
% \param f_main     focal length of the main lens in mm
% \param a_main     distance between main lens scene point in mm
%
% \return           distance between image side focus point and main lens 
%                   in mm
%
function b_main = calc_b_main_thick_lens(p_1, p_2, f_main, a_main)
    b_main = p_2 + 1/(1/(f_main-p_2) - 1/(a_main + p_1));
end