%
% \brief calc_b_main_thin_lens calculates the distance between virtual 
%                   image and main lens based on the thin lens equation
%
% \param f_main     focal length of the main lens in mm
% \param a_main     distance between main lens scene point in mm
%
% \return           distance between image side focus point and main lens 
%                   in mm
%
function b_main = calc_b_main_thin_lens(f_main, a_main)
    b_main = 1/(1/f_main - 1/a_main);
end