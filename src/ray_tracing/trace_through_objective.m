%
% \brief trace_through_objective traces a ray through the main lens
%
% \param p          ray point
% \param v          ray direction
% \param forward 	defines the direction - true: scene to cam, false: cam
%                   to scene
% \param start_idx 	defines the first lens surface to calculate an
%                   intersection with - required for starting the tracing 
%                   within the lens, e.g. for magnification calculations
%
% \return x         ray positions first coordinate
% \return y         ray positions second coordinate
% \return v         final ray direction
% \return blocked   true if blocked, false otherwise
%
function [x, y, v, blocked] = trace_through_objective(p,v, forward, ...
    start_idx)
    
    if nargin < 4
        start_idx = 1;
    end

    global aperture_index d_main IOR_forward IOR_backward ...
        num_lens_surfaces position radius sa;
    
    x = [p(1)];
    y = [p(2)];
    blocked = false;
    
    for i = start_idx:num_lens_surfaces
        if forward == false
            lens_idx = num_lens_surfaces + 1 - i;
            ior_ratio = IOR_backward(lens_idx);
        else
            lens_idx = i;
            ior_ratio = IOR_forward(lens_idx);
        end
        if lens_idx ~= aperture_index
            [p, v, blocked] = refraction(p, v, ior_ratio, position(lens_idx), radius(lens_idx), sa(lens_idx), forward);
            x=[x; p(1)];
            y=[y; p(2)];
            
            if blocked
                break
            end
        else
            blocked = check_aperture(p, v, d_main);
            if blocked
                break
            end
        end
    end
end
