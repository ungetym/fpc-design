%
% \brief trace_through_two_plane_MLA traces a ray through the MLA
%
% \param p              ray point
% \param v              ray direction
% \param forward        defines the direction - true: main lens to sensor, 
%                       false: sensor to main lens
% \param center_mode 	determines whether the real ML centers should be
%                       used:
%                       1 - dynamical ML center at intersection, 
%                       2 - always use center at zero
%                       3 - correct ML centers
%
% \return x             ray positions first coordinate
% \return y             ray positions second coordinate
% \return w             final ray direction
% \return blocked       true if blocked, false otherwise
%
function [x,y,w,blocked] = trace_through_two_plane_MLA(p, v, forward, ...
    center_mode)

    global b_main a_ML b_ML f_ML d_ML;
    d_objective_sensor = b_main + a_ML + b_ML;
    
    % these values are specific for the two plane approximation of the
    % Blender add-on
    IOR_MLA = 100;
    thickness_MLA = 0.05;
    
    x = [p(1)];
    y = [p(2)];
    
    q = [0; 0];
    w = [0; 0];
    blocked = false;
    
    back_MLA = d_objective_sensor - b_ML + thickness_MLA;
    front_MLA = d_objective_sensor - b_ML;
    
    if ~forward
    
        %%%%%%%%%%%%%%%%%%%%%%%  Back surface refraction  %%%%%%%%%%%%%%%%%

        % solve p+sv=(back_MLA,*)
        s = (back_MLA-p(1))/v(1);
        q = [back_MLA; p(2)+s*v(2)];
        % calculate incident angle
        signed_angle = calc_angle(v,[-1;0]);
        % apply Snell's law
        sin_of_angle = sin(abs(signed_angle)) / IOR_MLA;
        if abs(sin_of_angle) > 1.0 %reflection instead of transmission
            blocked = true;
            return;
        end

        new_angle = sign(signed_angle)*abs(asin(sin_of_angle));
        % calculate new direction from angle
        new_angle = new_angle + pi;
        w = [cos(new_angle); sin(new_angle)];

        x=[x; q(1)];
        y=[y; q(2)];

        %%%%%%%%%%%%%%%%%%%%%%  Front surface refraction  %%%%%%%%%%%%%%%%%

        % solve q+sw=(front_MLA,*)
        s = (front_MLA-q(1))/w(1);
        q = [front_MLA; q(2)+s*w(2)];

        % calculate normal based on thin lens approximation for MLA
        if center_mode == 1 % dynamical ML center
            center = [front_MLA + f_ML*(IOR_MLA-1); q(2)];
        elseif center_mode == 2 % use center at zero
            center = [front_MLA + f_ML*(IOR_MLA-1); 0];
        elseif center_mode == 3 % d_ML determines center
            ML_idx = floor((q(2) + 0.5 * d_ML) / d_ML);
            center = [front_MLA + f_ML*(IOR_MLA-1); ML_idx * d_ML];
        end
        normal = q-center;
        normal = normal/norm(normal);
        % calculate incident angle based on new normal
        signed_angle = calc_angle(w,normal);
        % apply Snell's law
        sin_of_angle = IOR_MLA * sin(abs(signed_angle));
        if abs(sin_of_angle) > 1.0 %reflection instead of transmission
            blocked = true;
            return;
        end
        new_angle = sign(signed_angle)*abs(asin(sin_of_angle));
        % calculate new direction from angle
        new_angle = new_angle + calc_angle(normal,[1,0]);
        w = [cos(new_angle); sin(new_angle)];
    
    else
        %%%%%%%%%%%%%%%%%%%%%%  Front surface refraction  %%%%%%%%%%%%%%%%%

        % solve p+sv=(front_MLA,*)
        s = (front_MLA-p(1))/v(1);
        q = [front_MLA; p(2)+s*v(2)];

        % calculate normal based on thin lens approximation for MLA
        if center_mode == 1 % dynamical ML center
            center = [front_MLA + f_ML*(IOR_MLA-1); q(2)];
        elseif center_mode == 2 % use center at zero
            center = [front_MLA + f_ML*(IOR_MLA-1); 0];
        elseif center_mode == 3 % d_ML determines center
            ML_idx = floor((q(2) + 0.5 * d_ML) / d_ML);
            center = [front_MLA + f_ML*(IOR_MLA-1); ML_idx * d_ML];
        end
        normal = q - center;
        normal = -normal/norm(normal);
        % calculate incident angle based on new normal
        signed_angle = calc_angle(v,normal);
        % apply Snell's law
        sin_of_angle = sin(abs(signed_angle)) / IOR_MLA;
        if abs(sin_of_angle) > 1.0 %reflection instead of transmission
            blocked = true;
            return;
        end
        new_angle = sign(signed_angle) * abs(asin(sin_of_angle));
        % calculate new direction from angle
        new_angle = new_angle + calc_angle(normal,[1,0]);
        w = [cos(new_angle); sin(new_angle)];
        
        x=[x; q(1)];
        y=[y; q(2)];
        
        %%%%%%%%%%%%%%%%%%%%%%%  Back surface refraction  %%%%%%%%%%%%%%%%%
    
        % solve q+sw=(back_MLA,*)
        s = (back_MLA-q(1))/w(1);
        q = [back_MLA; q(2)+s*w(2)];
        % calculate incident angle
        signed_angle = calc_angle(w,[1;0]);
        % apply Snell's law
        sin_of_angle = sin(abs(signed_angle)) * IOR_MLA;
        if abs(sin_of_angle) > 1.0 %reflection instead of transmission
            blocked = true;
            return;
        end

        new_angle = sign(signed_angle) * abs(asin(sin_of_angle));
        % calculate new direction from angle
        w = [cos(new_angle); sin(new_angle)];
    end
    
    x=[x; q(1)];
    y=[y; q(2)];
end