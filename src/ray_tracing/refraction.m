%
% \brief refraction calculates the refraction of a given ray at a spherical
%                   lens surface
%
% \param p          ray point
% \param v          ray direction
% \param IOR        ratio n_1/n_2 of current and next materials IORs
% \param center 	of spherical lens
% \param radius 	of spherical lens
% \param height 	of spherical lens
% \param forward 	defines the direction - true: scene to cam, false: cam
%                   to scene
%
% \return q         new ray position
% \return w         new ray direction
% \return blocked   true if blocked, false otherwise
%
function [q, w, blocked] = refraction(p, v, IOR, center, radius, height,...
    forward)
    
    q = [0; 0];
    w = [0; 0];
    blocked = false;
    
    % flip radius if negative
    flip = (radius < 0);
    if flip
        radius = - radius;
    end

    % ray hitting a spherical surface
    if radius > 0
        % calculate intersection of surface and ray
        p_c = [p(1) - center, p(2)];
        pc = dot(p_c,p_c);
        pcv = dot(p_c,v);
        squared = radius * radius - pc + pcv * pcv;
        if squared < 0.0
            blocked = true;
            return;
        end

        if flip
            lambda = - pcv - (-1)^forward * sqrt(squared);
        else
            lambda = - pcv + (-1)^forward * sqrt(squared);
        end

        q = p+lambda*v;
    end
        
    % ray hitting a flat surface
    if radius == 0
        q = [center; p(2) + (center-p(1))/v(1) * v(2)];
        lambda = 0;
    end
    
    if (lambda < 0) || (abs(q(2)) > height)
        blocked = true;
        return;
    end

    % calculate normal (pointing into camera)
    normal = [center - q(1); -q(2)];
    normal = normal/norm(normal);
    if flip
        normal = -normal;
    elseif radius == 0
        normal = [1; 0];
    end
    
    if forward == false
        normal = -normal;
    end

    signed_angle = calc_angle(v,normal);
    
    % apply Snell's law
    sin_of_angle = IOR * sin(abs(signed_angle));
    if abs(sin_of_angle) > 1.0 %reflection instead of transmission
        blocked = true;
        return;
    end

    new_angle = sign(signed_angle)*abs(asin(sin_of_angle));

    % calculate new direction from angle
    new_angle = new_angle + calc_angle(normal,[1,0]);
    w = [cos(new_angle); sin(new_angle)];
end
