% Lens file
lensfile = '../lens_data/D-Gauss7 F1.4 46deg_Yoshisato Fujioka USP4443070 p336.csv';
% Save file
savefile = 'result.csv';

% Desired parameters
a_main = 500;
f_ML = 1.5;
d_ML = 0.7;
lambda = 0.3;

pixel_size = 0.01;
sensor_height = 30;

% Call thin lens/thick lens/rt design function to calculate parameters. The
% last parameter 'method' specifies the used model (1=thin, 2=thick, no
% argument = rt)
auto_design_single_focus(a_main, f_ML, d_ML, lambda, pixel_size,...
    sensor_height, lensfile, savefile);